export const addToFavorites = (data) => {
  const currentFavorites = JSON.parse(
    localStorage.getItem("favorites") || "[]"
  );
  currentFavorites.push(data);
  localStorage.setItem("favorites", JSON.stringify(currentFavorites));
  return currentFavorites;
};
