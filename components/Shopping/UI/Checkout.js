import React, { useState } from "react";
import { useSelector } from "react-redux";
import { Http } from "../../../infrastucture/http/Http";
import { ShoppingCardSelector } from "../../../infrastucture/redux/shoppingCardReducer";
import { CheckoutStyles } from "./Checkout.styled";

const Checkout = ({ setIsCheckout }) => {
  const { card } = useSelector(ShoppingCardSelector);
  const [submitForm, setSubmitForm] = useState({
    name: "",
    email: "",
    cel: "",
    document_type: "",
    document: "",
    address: "",
    town: "",
    ref: "",
  });
  const onChangeSubmitForm = (e) => {
    setSubmitForm({ ...submitForm, [e.target.name]: e.target.value });
  };
  async function onSubmit(event) {
    event.preventDefault();
    const {
      data: { response },
    } = await Http.get("company");

    console.log({ client: submitForm, card, response });
  }
  return (
    <CheckoutStyles>
      <form onSubmit={onSubmit}>
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <label htmlFor="name"> Nombre completo</label>
              <input
                required
                name="name"
                onChange={onChangeSubmitForm}
                type="text"
              />
            </div>
            <div className="col-md-12">
              <label htmlFor="email">Correo electronico</label>
              <input
                required
                name="email"
                onChange={onChangeSubmitForm}
                type="email"
              />
            </div>
            <div className="col-md-12">
              <label htmlFor="cel">Celular</label>
              <input
                required
                name="cel"
                onChange={onChangeSubmitForm}
                type="number"
              />
            </div>
            <div className="col-md-12">
              <label htmlFor="document_type">Tipo de documento</label>
              <select
                required
                onChange={onChangeSubmitForm}
                name="document_type"
              >
                <option value="CC">Cedula de ciudadania</option>
                <option value="TI">Tarjeta de identidad</option>
                <option value="PP">Pasaporte</option>
              </select>
            </div>
            <div className="col-md-12">
              <label htmlFor="document">Documento de identidad</label>
              <input
                required
                onChange={onChangeSubmitForm}
                name="document"
                type="text"
              />
            </div>
            <div className="col-md-12">
              <label htmlFor="address">Dirección</label>
              <input
                required
                onChange={onChangeSubmitForm}
                name="address"
                type="text"
              />
            </div>
            <div className="col-md-12">
              <label htmlFor="town">barrio</label>
              <input
                required
                onChange={onChangeSubmitForm}
                name="town"
                type="text"
              />
            </div>
            <div className="col-md-12">
              <label htmlFor="ref">referencia</label>
              <input onChange={onChangeSubmitForm} name="ref" type="text" />
            </div>
            <div className="col-md-12">
              <button>Realizar Compra</button>
            </div>
            <div className="col-md-12">
              <button onClick={() => setIsCheckout(false)}>Cancelar</button>
            </div>
          </div>
        </div>
      </form>
    </CheckoutStyles>
  );
};

export default Checkout;
