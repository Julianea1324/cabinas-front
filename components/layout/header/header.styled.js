import styled from "styled-components";
export const Header = styled.div`
  .header__top {
    position: fixed;
    top: 0;
    width: 100%;
    height: 100px;
    left: 0px;
    background: rgba(66, 152, 227, 0.88);
    display: flex;
    align-items: center;
    justify-content: center;
    z-index: 3;
    .shopping {
      position: relative;
      div {
        position: absolute;
        left: -21%;
        top: -90%;
        z-index: 2;
        height: 15px;
        font-size: 9px;
        background: #00b8d3;
        color: white;
        font-weight: bolder;
        width: 15px;
        display: -webkit-box;
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
        -webkit-align-items: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        align-items: center;
        padding: 2px;
        -webkit-box-pack: center;
        -webkit-justify-content: center;
        -ms-flex-pack: center;
        justify-content: center;
        border-radius: 5px;
        font-family: "Roboto", sans-serif;
      }
    }
    .container {
      display: flex;
      justify-content: space-around;
      align-items: center;
      .search {
        display: flex;
        align-items: center;
        position: relative;
        min-width: 300px;
        .search-list {
          position: absolute;
          background: white;
          width: 100%;
          top: 50px;
          border-radius: 5px;
          padding: 10px 0;
          ul {
            flex-wrap: wrap;
            li {
              width: 100%;
              color: black;
            }
          }
        }
        i {
          background: #fff;
          border-radius: 0px 100px 100px 0px;
          position: absolute;
          right: 0px;
          width: 50px;
          height: 45px;
          display: flex;
          justify-content: center;
          align-items: center;
        }
        input {
          border-radius: 100px;
          border: none;
          padding: 10px 53px 10px 10px;
          position: relative;
          width: 283px;
          height: 45px;
          &:focus {
            outline: none;
          }
        }
      }
      ul {
        display: flex;
        margin: 0;

        li {
          list-style: none;
          margin-right: 10px;
          font-family: "Roboto", sans-serif;
          font-style: normal;
          font-weight: bolder;
          font-size: 13px;
          line-height: 18px;
          letter-spacing: 0.095em;
          color: #f9f9f9;
          margin-right: 2rem;
          text-transform: uppercase;
          cursor: pointer;
        }
      }
      .icons {
        i {
          margin-right: 1.5rem;
          cursor: pointer;
        }
      }
    }
  }
  .mobile {
    display: none;
  }
  .mobile-nav {
    .logo {
      position: relative;
      z-index: 7;
    }
    input + label {
      position: fixed;
      top: 40px;
      right: 40px;
      height: 20px;
      width: 50px;
      z-index: 5;
      span {
        position: absolute;
        width: 100%;
        height: 5px;
        top: 50%;
        margin-top: -1px;
        left: 0;
        display: block;
        background: white;
        transition: 0.5s;
      }
      span:first-child {
        top: -9px;
      }
      span:last-child {
        top: 30px;
      }
    }
    label:hover {
      cursor: pointer;
    }
    input:checked + label {
      span {
        opacity: 0;
        top: 50%;
      }
      span:first-child {
        opacity: 1;
        transform: rotate(405deg);
      }
      span:last-child {
        opacity: 1;
        transform: rotate(-405deg);
      }
    }
    input ~ nav {
      background: rgba(79, 151, 213, 0.92);
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100px;
      z-index: 3;
      transition: 0.5s;
      transition-delay: 0.5s;
      overflow: hidden;
      display: flex;
      justify-content: center;
      align-items: center;
      > ul {
        text-align: center;
        display: inline-block !important;
        > li {
          opacity: 0;
          transition: 0.5s;
          transition-delay: 0s;
          color: white;
          margin: 0 !important;
          > a {
            text-decoration: none;
            text-transform: uppercase;
            color: white;
            font-weight: 700;
            font-family: sans-serif;
            display: block;
            padding: 30px;
          }
        }
      }
    }
    input:checked ~ nav {
      height: 100%;
      transition-delay: 0s;
      > ul {
        > li {
          opacity: 1;
          transition-delay: 0.5s;
        }
      }
    }
    input {
      opacity: 0;
    }
  }
  @media (max-width: 600px) {
    .desktop {
      display: none;
    }
    .mobile {
      display: block;
    }
    .header__top {
      background-color: transparent !important;
    }
    .search {
      margin-top: 2rem;
      input {
        opacity: 1 !important;
      }
    }
  }
`;
