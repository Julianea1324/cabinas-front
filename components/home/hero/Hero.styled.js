import styled from "styled-components";

export const Hero = styled.div`
  background: url(/home/banner-principal.jpg);
  height: 681px;
  display: flex;
  align-items: center;
  .container {
    h3 {
      font-family: "Roboto", sans-serif;
      font-style: normal;
      font-weight: bold;
      font-size: 18px;
      line-height: 21px;
      text-transform: uppercase;
      color: rgba(255, 255, 255, 0.7);
      letter-spacing: 0.5em;
    }
    p {
      font-family: "Roboto", sans-serif;
      font-style: normal;
      font-weight: 900;
      font-size: 36px;
      line-height: 42px;
      width: 545px;
      color: #ffffff;
    }
    button {
      width: 202px;
      height: 45px;
      left: 244px;
      top: 407px;
      background: rgba(255, 255, 255, 0.16);
      border: 1px solid #ffffff;
      box-sizing: border-box;
      border-radius: 3px;
      color: white;
      text-transform: uppercase;
    }
  }
  @media (max-width: 600px) {
    p {
      width: 100%!important;
    }
  }
`;
