import React, { useEffect, useState } from "react";
import Image from "next/image";
import Link from "next/link";
import { useSelector } from "react-redux";
import { ShoppingCardSelector } from "../../../../infrastucture/redux/shoppingCardReducer";
import { Http } from "../../../../infrastucture/http/Http";
export const MobileNav = ({ isOpenShopping, setIsisOpenShopping }) => {
  const { card } = useSelector(ShoppingCardSelector);
  const [products, setProducts] = useState([]);
  const [allProducts, setAllProducts] = useState([]);
  useEffect(() => {
    async function products() {
      const res = await Http.get("products");
      const { response } = await res.data;
      setAllProducts(response);
    }
    products();
  }, []);
  const searchProducts = async (searching) => {
    setProducts([]);
    searching.length &&
      setProducts(
        allProducts.filter(({ display_name }) =>
          display_name.toLowerCase().includes(searching.toLowerCase())
        )
      );
  };
  return (
    <div className="header__top">
      <div className="container mobile-nav">
        <div className="logo">
          <Image src="/logo.png" width="245px" height="46px" />
        </div>
        <input id="burger" type="checkbox" />

        <label for="burger">
          <span></span>
          <span></span>
          <span></span>
        </label>
        <nav>
          <ul>
            <li>
              <Link href="/">inicio</Link>
            </li>
            <li>
              <Link href="/productos">productos</Link>
            </li>
            <li>
              <Link href="#footer">contacto</Link>
            </li>

            <li>
              <div className="icons">
                <Link href="/favoritos">
                  <i>
                    <Image src="/icon/like.svg" width="34px" height="34px" />
                  </i>
                </Link>

                <i
                  onClick={() => setIsisOpenShopping(!isOpenShopping)}
                  className="shopping"
                >
                  <div>{card.length}</div>
                  <Image src="/icon/carrito 1.svg" width="30px" height="34px" />
                </i>
                <i>
                  <Image
                    src="/icon/whatsapp 1.svg"
                    width="30px"
                    height="34px"
                  />
                </i>
              </div>
            </li>
            <div className="search">
              <input
                type="text"
                placeholder="Buscar"
                onChange={({ target }) => searchProducts(target.value)}
              />
              <i>
                <Image src="/icon/search.svg" width="24px" height="24px" />
              </i>
              {products.length && (
                <div className="search-list">
                  <ul>
                    {products.map(({ display_name, id }) => (
                      <li key={`searching-${id}`}>
                        <Link href={`/producto/${id}`}>{display_name}</Link>
                      </li>
                    ))}
                  </ul>
                </div>
              )}
            </div>
          </ul>
        </nav>
      </div>
    </div>
  );
};

{
  /* <ul>
<li>
  <Link href="/">inicio</Link>
</li>
<li>
  <Link href="/productos">productos</Link>
</li>
<li>
  <Link href="#footer">contacto</Link>
</li>
</ul>
<div className="search">
<input
  type="text"
  placeholder="Buscar"
  onChange={({ target }) => searchProducts(target.value)}
/>
<i>
  <Image src="/icon/search.svg" width="24px" height="24px" />
</i>
{products.length && (
  <div className="search-list">
    <ul>
      {products.map(({ display_name, id }) => (
        <li key={`searching-${id}`}><Link href={`/producto/${id}`}>{display_name}</Link></li>
      ))}
    </ul>
  </div>
)}
</div>
<div className="icons">
<Link href="/favoritos">
  <i>
    <Image src="/icon/like.svg" width="34px" height="34px" />
  </i>
</Link>

<i
  onClick={() => setIsisOpenShopping(!isOpenShopping)}
  className="shopping"
>
  <div>{card.length}</div>
  <Image src="/icon/carrito 1.svg" width="30px" height="34px" />
</i>
<i>
  <Image src="/icon/whatsapp 1.svg" width="30px" height="34px" />
</i>
</div>
</div> */
}
