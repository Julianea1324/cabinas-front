import styled from "styled-components";

export const BoxProductsStyle = styled.div`
  position: relative;
  .info {
    position: absolute;
    top: 80%;
    left: 6%;

    p {
      margin: 0;
      font-family: "Roboto", sans-serif;
      font-style: normal;
      font-weight: 900;
      font-size: 15px;
      line-height: 18px;
      /* identical to box height */
      text-transform: capitalize;
      color: #ffffff;
    }
  }

  .recentBox {
      border-radius: 10px;
    height: 255px;
    background-size: contain;
    background-repeat: no-repeat;
    background-position: 50%;
    position: relative;

    &::after {
        border-radius: 10px;
      position: absolute;
      content: "";
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      background-color: #9a9a9a69;
    }
  }
  @media (max-width: 600px) {
    .recentBox {
     margin: 5px 0;
    }
  }
`;
