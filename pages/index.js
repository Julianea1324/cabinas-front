import { Http } from "../infrastucture/http/Http";
import HeroUI from "../components/home/hero";
import { Layout } from "../components/layout";
import FeaturedProductsComponent from "../components/home/FeaturedProducts/Index";
import RecentProductsComponent from "../components/home/recentProducts";

export default function Home({ products }) {
  return (
    <>
      <Layout title="Inicio">
        <HeroUI />
        <FeaturedProductsComponent products={products} />
        <RecentProductsComponent products={products} />
      </Layout>
    </>
  );
}

export async function getServerSideProps() {
  const res = await Http.get("products");
  const data = await res.data;
  return { props: { products: data.response } };
}
