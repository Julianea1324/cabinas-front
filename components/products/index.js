import React from "react";
import { BoxProducts } from "../ProductBox/BoxProducts";
import { ProductStyle } from "./products.styled";
import { Filter } from "./UI/Filter";

export const ProductComponent = ({ products, hasFilter = false }) => {
  return (
    <ProductStyle>
      <div className="container">
        <div className="row">
          {/* {hasFilter && <Filter />} */}
          {products.map((e) => (
            <BoxProducts {...e} key={`product-component-${e.id}`} />
          ))}
          {
            !products.length && <div className="no-products">No hay productos agregados.</div>
          }
        </div>
      </div>
    </ProductStyle>
  );
};
