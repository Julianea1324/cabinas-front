import React from "react";
import { useState } from "react";
import ShoppingCard from "../Shopping";
import { FooterUI } from "./footer";
import { HeaderUi } from "./header";

export const Layout = ({ children, title = "cabinas" }) => {
  const [isOpenShopping, setIsisOpenShopping] = useState(false);
  return (
    <>
      <ShoppingCard
        isOpenShopping={isOpenShopping}
        setIsisOpenShopping={setIsisOpenShopping}
      />
      <HeaderUi
        title={title}
        isOpenShopping={isOpenShopping}
        setIsisOpenShopping={setIsisOpenShopping}
      />
      <div>{children}</div>
      <FooterUI
        setIsisOpenShopping={setIsisOpenShopping}
        isOpenShopping={isOpenShopping}
      />
    </>
  );
};
