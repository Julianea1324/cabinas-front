import Head from "next/head";
import React from "react";
import { Header } from "./header.styled";
import { MobileNav } from "./ui/Mobile";
import { Nav } from "./ui/Nav";
export const HeaderUi = ({ title, isOpenShopping, setIsisOpenShopping }) => {
  return (
    <Header>
      <Head>
        <title>Cabinas | {title}</title>
      </Head>
      <div className="mobile">
        <MobileNav
          isOpenShopping={isOpenShopping}
          setIsisOpenShopping={setIsisOpenShopping}
        />
      </div>
      <div className="desktop">
        <Nav
          isOpenShopping={isOpenShopping}
          setIsisOpenShopping={setIsisOpenShopping}
        />
      </div>
    </Header>
  );
};
