import React, { useState } from "react";
import Slider from "./UI/slider";
import { InternalProduct } from "./internalProducts.styled";
import { useDispatch, useSelector } from "react-redux";
import { addShopping, ShoppingCardSelector } from "../../infrastucture/redux/shoppingCardReducer";
const renderHTML = (rawHTML) =>
  React.createElement("div", { dangerouslySetInnerHTML: { __html: rawHTML } });

export const InternalProductComponent = ({
  display_name,
  first_img,
  second_img,
  third_img,
  quarter_img,
  funnel,
  id,
  price,
  qty,
  ...data
}) => {
  const currentImages = [first_img, second_img, third_img, quarter_img];
  const { card } = useSelector(ShoppingCardSelector);
  const dispatch = useDispatch();
  function addToShopping() {
    dispatch(
      addShopping({  display_name,
        first_img,
        second_img,
        third_img,
        quarter_img,
        funnel,
        id,
        price,
        qty, ...data })
    );
  }
  return (
    <InternalProduct>
      <div className="container">
        <div className="row">
          <div className="col-md-6">
            <Slider images={currentImages} />
          </div>
          <div className="col-md-6 information">
            <h2>{display_name}</h2>
            {renderHTML(funnel)}
            <div className="price">
              ${price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
            </div>
            {!card.some(({ id: currentId }) => currentId === id) ? (
              <button onClick={addToShopping}>Comprar Ahora</button>
            ): <div className="addded-shopping">Producto agregado al carrito.</div>}
          </div>
        </div>
      </div>
    </InternalProduct>
  );
};
