import styled from "styled-components";

export const Footer = styled.div`
  background-color: red;
  height: 334px;
  background: rgba(66, 152, 227, 0.88);
  display: flex;
  justify-content: center;
  align-items: center;
  .icons,
  .social {
    ul {
      margin: 0;
      li {
        i {
          margin-right: 10px;
        }
        cursor: pointer;
        list-style: none;
        font-family: "Roboto", sans-serif;
        font-style: normal;
        font-weight: bold;
        font-size: 15px;
        line-height: 18px;
        letter-spacing: 0.095em;
        color: white;
        text-transform: uppercase;
        margin-top: 10px;
        display: flex;
        justify-content: start;
        align-items: center;
      }
    }
  }
  .location {
    color: white;
    display: flex;
    justify-content: center;
    align-items: center;
    i {
      margin-right: 1rem;
    }
  }
  .menu {
    margin: 0;
    li {
      font-family: "Roboto", sans-serif;
      font-style: normal;
      font-weight: bold;
      font-size: 15px;
      line-height: 18px;
      letter-spacing: 0.095em;
      color: white;
      text-transform: uppercase;
      margin-top: 10px;
    }
  }
  @media (max-width: 600px) {
    height: auto !important;
    padding: 1rem;
    ul{
      margin: 0;
      padding: 0;
    }
    .col-md-3{
      display: flex;
      justify-content: center;
      margin: 10px 0;
    }
  }
`;
