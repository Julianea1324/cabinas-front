import React from "react";
import { BoxProductsStyle } from "./BoxProducts.styled";

export const BoxProductsRecent = ({ display_name, first_img, price }) => {
  return (
    <div className="col-md-4">
      <BoxProductsStyle>
        <div
          className="recentBox"
          style={{ backgroundImage: `url(${first_img})` }}
        ></div>
        <div className="info">
          <p className="name">{display_name}</p>
          <p className="price">${price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}</p>
        </div>
      </BoxProductsStyle>
    </div>
  );
};
