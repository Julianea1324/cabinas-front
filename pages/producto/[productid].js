import React from "react";
import FeaturedProductsComponent from "../../components/home/FeaturedProducts/Index";
import { InternalProductComponent } from "../../components/internalProduct";
import { ProductHeaderUI } from "../../components/internalProduct/UI/Header";
import { Layout } from "../../components/layout";
import { Http } from "../../infrastucture/http/Http";

const Product = ({ currentProduct,products }) => {
  return (
    <>
      <Layout title={'currenProduct'}>
        <ProductHeaderUI />
        <InternalProductComponent {...currentProduct}/>
        <FeaturedProductsComponent products={products} />
      </Layout>
    </>
  );
};
export async function getServerSideProps({ params }) {
  const products = await Http.get("products");
  const currentProduct = await Http.get(`products/${params.productid}`);
  const data = await products.data;
  return { props: { products: data.response, currentProduct:currentProduct.data.response } };
}

export default Product;
