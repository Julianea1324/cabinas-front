import styled from "styled-components";

export const ShoppingCardStyle = styled.div`
  .show {
    right: 0%;
    transition: all 1s;
  }
  .hide {
    right: -100%;
    transition: all 1s;
  }
  button {
    box-sizing: border-box;
    border: none;
    background: white;
    border-radius: 3px;
    color: rgba(66, 152, 227);;
    text-transform: uppercase;
    font-weight: bold;
    padding: 10px;
    margin-top: 10px;
    width: 100%;
  }
  .shopping {
    position: fixed;
    width: 300px;
    height: 87vh;
    overflow: auto;
    background: rgba(66, 152, 227, 0.88);
    z-index: 4;
    top: 100px;
    padding: 1rem;
    .total{
      color: white;
      font-weight: bolder;
    }
    
    &-close {
      position: absolute;
      color: rgba(66, 152, 227);
      font-weight: bolder;
      cursor: pointer;
      right: 1rem;
      background-color: white;
      padding: 5px;
      height: 30px;
      width: 30px;
      border-radius: 3px;
      display: flex;
      justify-content: center;
      align-items: center;
      z-index: 4;
    }
    &-title {
      font-weight: bold;
      color: white;
      text-transform: uppercase;
      position: relative;
      margin-bottom: 1rem;
      &::after {
        content: "";
        position: absolute;
        width: 40%;
        height: 2px;
        border-radius: 3px;
        background-color: white;
        left: 0;
        bottom: -4px;
      }
    }
  }
  @media (max-width: 600px) {
    .shopping {
      width: 100%!important;
    }
  
  }
`;
