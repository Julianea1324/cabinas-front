import styled from 'styled-components'

export const RecentProducts = styled.div`
margin: 3rem 0 6rem 0;
  h1 {
    font-family: "Roboto", sans-serif;
    font-style: normal;
    font-weight: 900;
    font-size: 18px;
    line-height: 21px;
    color: #373737;
    text-transform: uppercase;
    margin: 2rem 0;
  }
`