import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  card: [],
};

export const ShoppingCardSlice = createSlice({
  name: "shopping",
  initialState,
  reducers: {
    addShopping: (state, { payload }) => {
      state.loading = true;
      state.error = false;
      state.card = !state.card.some(({ id }) => id === payload.id) ? [
        ...state.card,
        payload,
      ] : state.card;
    },
    removeShopping: (state, { payload }) => {
      state.loading = true;
      state.error = false;
      state.card = state.card.filter(({ id }) => id !== payload);
    },
  },
});

// Action creators are generated for each case reducer function
export const { addShopping, removeShopping } = ShoppingCardSlice.actions;
export const ShoppingCardSelector = (state) => state.shopping;
export default ShoppingCardSlice.reducer;
