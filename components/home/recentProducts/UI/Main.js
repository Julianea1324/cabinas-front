import React from "react";
import { BoxProductsRecent } from "./BoxProductsRecent";

export const MainRecentProductsUI = ({ products }) => (
  <section id="recentProducts">
    <div className="container">
      <h1>PRODUCTOS RECIENTES</h1>
      <div className="row">
        {products.slice(Math.max(0, products.length - 3)).map((e) => (
          <BoxProductsRecent {...e}  key={`recent-prodcut-${e.id}`}/>
        ))}
      </div>
    </div>
  </section>
);
