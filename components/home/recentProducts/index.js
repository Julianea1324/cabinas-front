import React from "react";
import { RecentProducts } from "./RecentProducts.styled";
import { MainRecentProductsUI } from "./UI/Main";

const RecentProductsComponent = ({ products }) => {
  return (
    <RecentProducts>
      <MainRecentProductsUI products={products} />
    </RecentProducts>
  );
};

export default RecentProductsComponent;
