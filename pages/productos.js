import React from "react";
import { ProductHeaderUI } from "../components/internalProduct/UI/Header";
import { Layout } from "../components/layout";
import { ProductComponent } from "../components/products";
import { Http } from "../infrastucture/http/Http";

const productos = ({products}) => {
  return (
    <>
      <Layout title="Productos">
        <ProductHeaderUI />
        <ProductComponent products={products} hasFilter/>
      </Layout>
    </>
  );
};
export async function getServerSideProps() {
  const res = await Http.get("products");
  const data = await res.data;
  return { props: { products: data.response } };
}
export default productos;
