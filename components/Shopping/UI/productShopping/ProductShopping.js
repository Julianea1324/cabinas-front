import Link from "next/link";
import Image from "next/image";
import React from "react";
import { BoxProductsS } from "./ProductShopping.styled";
import { useDispatch } from "react-redux";
import {
  addShopping,
  removeShopping,
} from "../../../../infrastucture/redux/shoppingCardReducer";

export const ProductShopping = ({
  display_name,
  first_img,
  price,
  slug,
  id,
}) => {
  const dispatch = useDispatch();

  function removeProductShopping() {
    dispatch(removeShopping(id));
  }
  return (
    <div className="col-md-12">
      <BoxProductsS>
        <div className="buttons-products">
          <ul>
            <li>
              <div onClick={removeProductShopping}>X</div>
            </li>
          </ul>
        </div>
        <Link href={`/producto/${id}`}>
          <div className="product-content">
            <div className="image">
              <Image src={first_img} width="298" height="365" />
            </div>
            <div className="info">
              <p className="name">{display_name}</p>
              <p className="price">
                ${price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
              </p>
            </div>
          </div>
        </Link>
      </BoxProductsS>
    </div>
  );
};
