import styled from "styled-components";

export const FilterStyle = styled.div`
  .filterBackground {
    height: 55px;
    background: #ebebeb;
    border-radius: 3px;
    margin-bottom: 10px;
    padding: 0 2rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    ul {
      display: flex;
      margin: 0;
      padding: 0;
      justify-content: center;
      li {
        font-family: "Roboto", sans-serif;
        font-style: normal;
        font-weight: 900;
        list-style: none;
        color: #5C5C5C;
        margin-right: 10px;
      }
    }
  }
`;
