import React from "react";
import { BannerStyles } from "./Banned.styled";
export const Banners = () => {
  return (
    <BannerStyles>
      <div className="container">
        <div className="row">
          <div className="content">
            <img src="/home/banner-1.png" />
            <div className="info">
              Lorem ipsum dolor sit amet consectetur adipisicing elit
            </div>
          </div>
          <div className="content">
            <img src="/home/banner-2.png" />
            <div className="info">
              Lorem ipsum dolor sit amet consectetur adipisicing elit
            </div>
          </div>
        </div>
      </div>
    </BannerStyles>
  );
};
