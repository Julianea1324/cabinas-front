import React, { useEffect, useState } from "react";
import { Footer } from "./Footer.styled";
import Image from "next/image";
import Link from "next/link";
import { Http } from "../../../infrastucture/http/Http";
export const FooterUI = ({ setIsisOpenShopping, isOpenShopping }) => {
  const [company, setCompany] = useState({});
  useEffect(() => {
    async function companys() {
      const {
        data: { response },
      } = await Http.get("company");

      return response;
    }
    companys().then((e) => setCompany(e));
  }, []);
  console.log(company);
  return (
    <Footer>
      <div id="footer">
        <div className="container">
          <div className="row">
            <div className="col-md-3">
              <ul className="menu">
                <li>
                  <Link href="/">inicio</Link>
                </li>
                <li>
                  <Link href="/productos">productos</Link>
                </li>
                <li>
                  {" "}
                  <Link href="#footer">contacto</Link>
                </li>
              </ul>
            </div>
            <div className="col-md-3">
              <div className="location">
                <i>
                  <Image src="/icon/locacion.svg" width="34px" height="34px" />
                </i>
                <p>Cras vitae lacus lorem. Phasellus convallis elit</p>
              </div>
            </div>
            <div className="col-md-3">
              <div className="social">
                <ul>
                  <a href={company.instagram} target="_blank">
                    <li>
                      <i>
                        <Image
                          src="/icon/instagram.svg"
                          width="34px"
                          height="34px"
                        />
                      </i>
                      <span>INSTACABINAS</span>
                    </li>
                  </a>

                  <a href={company.facebook} target="_blank">
                    <li>
                      <i>
                        <Image
                          src="/icon/facebook.svg"
                          width="34px"
                          height="34px"
                        />
                      </i>
                      <span>INSTA2CABINAS</span>
                    </li>
                  </a>

                  <a href="http://google.com" target="_blank">
                    <li>
                      <i>
                        <Image
                          src="/icon/whatsapp 1.svg"
                          width="34px"
                          height="34px"
                        />
                      </i>
                      <span>+{company.phone}</span>
                    </li>
                  </a>
                </ul>
              </div>
            </div>
            <div className="col-md-3">
              <div className="icons">
                <ul>
                  <Link href="/favoritos">
                    <li>
                      <i>
                        <Image
                          src="/icon/like.svg"
                          width="34px"
                          height="34px"
                        />
                      </i>
                      <span>Favoritos</span>
                    </li>
                  </Link>
                  <li onClick={() => setIsisOpenShopping(!isOpenShopping)}>
                    <i>
                      <Image
                        src="/icon/carrito 1.svg"
                        width="34px"
                        height="34px"
                      />
                    </i>
                    <span>Carrito</span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Footer>
  );
};
