import styled from "styled-components";

export const InternalProduct = styled.div`
  margin-top: 3rem;
  h2 {
    text-transform: uppercase;
  }
  button {
    box-sizing: border-box;
    border: none;
    background: #00b8d3;
    border-radius: 3px;
    color: white;
    text-transform: uppercase;
    font-weight: bold;
    padding: 10px;
    margin-top: 10px;
  }
  .information {
    padding-left: 2rem;
    .price {
      font-size: 1.5rem;
      font-weight: bold;
    }
  }
  .addded-shopping{
    background: rgba(66,152,227,0.88);
    border-radius: 6px;
    padding: 10px;
    text-transform: uppercase;
    color: white;
    font-weight: bolder;
    width: auto;
    margin-top: 2rem;
  }
`;
