import Link from "next/link";
import Image from "next/image";
import React, { useEffect, useState } from "react";
import { BoxProductsS } from "./BoxProduct.styled";
import { useDispatch, useSelector } from "react-redux";
import {
  addShopping,
  ShoppingCardSelector,
} from "../../infrastucture/redux/shoppingCardReducer";
import { addToFavorites } from "../../utils/addFavorites";

export const BoxProducts = ({
  display_name,
  first_img,
  price,
  slug,
  id,
  ...data
}) => {
  const dispatch = useDispatch();
  const [products, setProducts] = useState([]);
  const { card } = useSelector(ShoppingCardSelector);
  function addToShopping() {
    dispatch(
      addShopping({ display_name, first_img, price, slug, id, ...data })
    );
  }
  useEffect(() => {
    setProducts(JSON.parse(localStorage.getItem("favorites")));
  }, []);
  return (
    <div className="col-md-3">
      <BoxProductsS>
        <div className="buttons-products">
          <ul>
            {!products?.some(({ id: currentId }) => currentId === id) && (
              <li>
                <Image
                  src="/icon/like.svg"
                  width="34px"
                  height="34px"
                  onClick={() => {
                    addToFavorites({
                      display_name,
                      first_img,
                      price,
                      slug,
                      id,
                      ...data,
                    });
                    setProducts(JSON.parse(localStorage.getItem("favorites")));
                  }}
                />
              </li>
            )}

            {!card.some(({ id: currentId }) => currentId === id) && (
              <li>
                <Image
                  src="/icon/carrito 1.svg"
                  width="30px"
                  height="34px"
                  onClick={addToShopping}
                />
              </li>
            )}
          </ul>
        </div>
        <Link href={`/producto/${id}`}>
          <div className="product-content">
            <div className="image">
              <Image src={first_img} width="298" height="365" />
            </div>
            <div className="info">
              <p className="name">{display_name}</p>
              <p className="price">
                ${price?.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
              </p>
            </div>
          </div>
        </Link>
      </BoxProductsS>
    </div>
  );
};
