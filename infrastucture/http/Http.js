import axios from "axios";

export const Http = axios.create({
  baseURL: `https://tiendas.somoscreandola.com/api/`,
  headers: {
    dna: 1,
    cion: 'https://somoscreandola.com/'
  },
  timeout: 10000,
});

Http.interceptors.response.use(
  (resp) => {
    return resp;
  },
  (error) => {
    return Promise.reject(error);
  }
);
