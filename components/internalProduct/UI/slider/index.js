import React, { useEffect, useState } from "react";
import { SliderStyles } from "./slider.styled";
const Slider = ({ images }) => {
  const [currentImage, setCurrentImage] = useState(images[0]);
  useEffect(()=>{
    setCurrentImage(images[0])
  },[images])
  return (
    <SliderStyles>
      <img src={currentImage} className='currentImage' />
      <div className="imgChoise">
        {images.map((image,index) => (
          <div className="smallImage" key={`slider-img-${index}`}>
            <img src={image} onClick={()=>setCurrentImage(images[index])} />
          </div>
        ))}
      </div>
    </SliderStyles>
  );
};

export default Slider;
