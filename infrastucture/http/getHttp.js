import { Http } from "./Http";
export async function getHttp(url) {
  try {
    const res = await Http.get(url);
    const data = await res.data;
    return data;
  } catch (error) {
    return error;
  }
}
