import { configureStore } from '@reduxjs/toolkit'
import shoppingCardReducer from './shoppingCardReducer'

export const store = configureStore({
  reducer: {
      shopping: shoppingCardReducer
  },
})