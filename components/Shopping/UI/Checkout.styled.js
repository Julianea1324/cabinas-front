import styled from "styled-components";

export const CheckoutStyles = styled.div`
background-color: white;
border-radius: 5px;
padding: 10px 0;
button{
    background: rgba(66, 152, 227, 0.88)!important;
    color: white!important;
}
  label {
    width: 100%;
    margin-bottom: 5px;
    text-transform: uppercase;
  }
  select,
  input{
      width: 100%;
      padding: 5px;
      margin-bottom: 5px;
      text-transform: uppercase;
  }
`;
