import React from "react";
import { FeaturedProducts } from "./featuredProducts.styled";
import { MainFeatureProductsUI } from "./UI/Main";

const FeaturedProductsComponent = ({ products }) => {
  return (
    <FeaturedProducts>
      <MainFeatureProductsUI products={products} />
    </FeaturedProducts>
  );
};

export default FeaturedProductsComponent;
