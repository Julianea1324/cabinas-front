import React, { useState } from "react";
import { useSelector } from "react-redux";
import { ShoppingCardSelector } from "../../../infrastucture/redux/shoppingCardReducer";
import Checkout from "./Checkout";
import { ProductShopping } from "./productShopping/ProductShopping";
import { ShoppingCardStyle } from "./ShoppingCard.styled";

export const ShoppingCard = ({ isOpenShopping, setIsisOpenShopping }) => {
  const { card } = useSelector(ShoppingCardSelector);
  const [isCheckout, setIsCheckout] = useState(false);
  const total = card.reduce(
    (previousValue, currentValue) => previousValue + Number(currentValue.price),
    0
  );

  return (
    <ShoppingCardStyle>
      <div className={`shopping ${isOpenShopping ? "show" : "hide"}`}>
        <div
          className="shopping-close"
          onClick={() => setIsisOpenShopping(false)}
        >
          x
        </div>
        {isCheckout ? (
          <>
            <div className="shopping-title">Pasarela de pagos</div>
            <Checkout setIsCheckout={setIsCheckout}/>
          </>
        ) : (
          <>
          <div className="shopping-title">Carrito de compras</div>
            <div className="shopping-products">
              {card.map((card) => (
                <ProductShopping
                  {...card}
                  key={`product-shopping-${card.id}`}
                />
              ))}
              <div className="total">
                Total: ${" "}
                {total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")}
              </div>
              {!!total && (
                <button onClick={() => setIsCheckout(true)}>
                  comprar ahora
                </button>
              )}
            </div>
          </>
        )}
      </div>
    </ShoppingCardStyle>
  );
};
