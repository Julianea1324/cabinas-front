import React, { useEffect, useState } from "react";
import { ProductHeaderUI } from "../components/internalProduct/UI/Header";
import { Layout } from "../components/layout";
import { ProductComponent } from "../components/products";

const productos = () => {
  const [products, setProducts] = useState([]);
  useEffect(() => {
    setProducts(JSON.parse(localStorage.getItem("favorites")));
  }, []);

  return (
    <>
      <Layout title="Favoritos">
        <ProductHeaderUI />
        <ProductComponent products={products || []} />
      </Layout>
    </>
  );
};

export default productos;
