import styled from "styled-components";

export const ProductHeaderStyle = styled.div`
  background-image: url(/product/product-banner.jpg);
  height: 294px;
  background-size: cover;
`;
