import React, { useState } from "react";
import { ShoppingCard } from "./UI/ShoppingCard";

const ShoppingCardComponent = ({isOpenShopping, setIsisOpenShopping}) => {
  return (
    <ShoppingCard
      isOpenShopping={isOpenShopping}
      setIsisOpenShopping={setIsisOpenShopping}
    />
  );
};

export default ShoppingCardComponent;
