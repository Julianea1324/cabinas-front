import styled from 'styled-components'

export const ProductStyle = styled.div`
margin: 3rem 0;
.no-products{
    background: rgba(66,152,227,0.88);
    border-radius: 6px;
    padding: 10px;
    text-transform: uppercase;
    color: white;
    font-weight: bolder;
}
`