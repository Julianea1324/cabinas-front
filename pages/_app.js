import React from "react";
import { store } from "../infrastucture/redux/store";
import "../styles/globals.css";
import { Provider } from "react-redux";

const _app = ({ Component, pageProps }) => {
  return (
    <>
      <Provider store={store}>
        <Component {...pageProps} />
      </Provider>
    </>
  );
};

export default _app;
