import styled from "styled-components";

export const BannerStyles = styled.div`
  display: flex;
  align-items: center;
  margin-top: -1rem;
  .row {
    display: flex;
    justify-content: space-between;
    align-items: center;
    .content {
      position: relative;
      border-radius: 3px;
      padding: 0;
      width: 49%;
      &::after {
        content: "";
        position: absolute;
        left: 0%;
        right: 0%;
        top: 0%;
        bottom: 0%;
        width: 100%;
        height: 100%;
        background: linear-gradient(
          253.02deg,
          rgba(81, 171, 255, 0.11) 36.8%,
          rgba(24, 118, 204, 0.88) 102.96%
        );
        border-radius: 3px;
      }
      .info {
        position: absolute;
        bottom: 10px;
        z-index: 2;
        left: 10px;
        color: white;
        font-weight: bold;
        width: 50%;
      }
      img {
        width: 100%;
      }
    }
  }
  @media (max-width: 600px) {
    .row {
      justify-content: center;
      .content {
        width: 95% !important;
        margin-bottom: 1rem;
      }
    }
  }
`;
