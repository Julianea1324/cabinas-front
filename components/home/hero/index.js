import React, { useEffect } from "react";
import { Http } from "../../../infrastucture/http/Http";
import { Hero } from "./Hero.styled";
import { Banners } from "./UI/Banners";

const HeroUI = () => {
  return (
    <>
      <Hero>
        <div className="container">
          <h3>lorem ipsum</h3>
          <p>
            Pellentesque vitae lectus maximus, congue sapien ac, maximus massa.
          </p>
          <button>solicitar asesoria</button>
        </div>
      </Hero>
      <Banners />
    </>
  );
};

export default HeroUI;
