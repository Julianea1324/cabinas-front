import styled from "styled-components";

export const SliderStyles = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  .currentImage {
    width: 500px;
    height: 500px;
    margin-right: 10px;
  }
  .smallImage {
    height: 100px;
    width: 100px;
    img {
      width: 100%;
      cursor: pointer;
    }
  }
`;
