import styled from "styled-components";

export const BoxProductsS = styled.div`
  overflow: hidden;
  position: relative;
  &:hover {
    .buttons-products {
      left: 0%;
      transition: all 0.5s;
    }
  }
  .product-content {
    height: 445px;
    background: #ffffff;
    border: 1px solid #e8e8e8;
    box-sizing: border-box;
    border-radius: 3px;
    margin-bottom: 1rem;
    display: flex;
    flex-wrap: wrap;
    position: relative;
    cursor: pointer;
  }
  .buttons-products {
    position: absolute;
    border-radius: 0 0 10px 0;
    left: -100%;
    z-index: 2;
    background: rgba(66, 152, 227, 0.88);
    padding: 10px;
    display: flex;
    justify-content: flex-end;
    transition: all 0.5s;
    ul {
      display: flex;
      margin: 0;
      padding: 0;
      align-items: center;
      li {
        list-style: none;
        margin-left: 10px;
      }
    }
    img {
      cursor: pointer;
    }
  }
  .info {
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    padding: 1rem;
    p {
      margin: 0;
      width: 100%;
    }
    .name {
      font-family: "Roboto", sans-serif;
      font-style: normal;
      font-weight: 900;
      font-size: 15px;
      line-height: 18px;
      /* identical to box height */
      color: #5c5c5c;
      text-transform: capitalize;
    }
    .price {
      font-family: "Roboto", sans-serif;
      font-style: normal;
      font-weight: 900;
      font-size: 15px;
      line-height: 18px;
      /* identical to box height */
      color: #b7b7b7;
    }
  }
  @media (max-width: 600px) {
    .product-content {
      justify-content: center;
    }
  }
`;
