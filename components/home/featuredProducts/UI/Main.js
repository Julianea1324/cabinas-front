import React from "react";
import { BoxProducts } from "../../../ProductBox/BoxProducts";

export const MainFeatureProductsUI = ({products}) => (
    <section id="featuredProducts">
      <div className="container">
        <h1>Productos destacados</h1>
        <div className="row">
          {products.slice(Math.max(0, products.length - 4)).map((e) => (
            <BoxProducts {...e} key={`product-${e.id}`} />
          ))}
        </div>
      </div>
    </section>
  );
